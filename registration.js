const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const port =  process.env.PORT || 23877
const router = express.Router();
const InitiateMongoServer = require("./app/config/db");
const appRoutes = require('./app/routes/api')(router);

// Create the database connection
InitiateMongoServer();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// enable All CORS Requests
app.use(cors());

//  Connect all our routes to our application
app.use('/api', appRoutes);

// Turn on that server!
app.listen(port, () => {
	console.log(`server is successfully running on port ${port}` );
});
