var mongoose = require('mongoose'); // Import Mongoose Package
var Schema   = mongoose.Schema; // Assign Mongoose Schema to variable

var VendorRegistrationSchema = new Schema({
  registrationNumber: { type: String, require: true },
  userType: { type: String, required: true },
  fullName: { type: String, required: true, lowercase: true },
  email: { type: String, required: true, lowercase: true, unique: true },
  password: {type: String, required: true },
  mobileNumber: { type: String, required: true },
  companyName: { type: String, required: true },
  companyDetails: { type: Object, require: true, default: ''},
  bankDetails: { type: Object, require: true, default: ''},
  uploadDocumentsPath: { type: String, require: true, default: null },
  otpsecret: { type: String, require: true, default: '' },
  hsnGst: { type: Array, require: true, default: undefined },
  created_at: { type: Date, required: true, default: Date.now }
});

module.exports = mongoose.model('VendorRegistration', VendorRegistrationSchema);
