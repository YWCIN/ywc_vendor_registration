var mongoose = require('mongoose'); // Import Mongoose Package
var Schema   = mongoose.Schema; // Assign Mongoose Schema to variable

var VendorvalidationSchema = new Schema({
    registrationNumber: { type: String, required: true },
    gstDetails: { type: Object, required: true },    
    created_at: { type: Date, required: true, default: Date.now }
});

module.exports = mongoose.model('VendorDocVerification', VendorvalidationSchema);
