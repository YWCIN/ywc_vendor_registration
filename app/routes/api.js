// Bring in our dependencies
const axios = require('axios');
const multer = require('multer');
const fs = require('fs');
require('dotenv').config();
const uniqid = require('uniqid');
const speakeasy = require('speakeasy');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

// Import model
const VendorRegData = require('../models/vendorRegistration');
const Vendordetails = require('../models/vendorDocVerification');

module.exports = (router) => {
  // function to generate OTP
  const generateOtp = function generateOtp(secret) {
    let token = speakeasy.totp({
      secret:secret,
      encoding: 'base32',
      digits:6,
      step: 60,
      window:2
    });
    return token;
  }

  // Router for user registration
  router.post('/vendorRegistration', async(req, res) => {
    try {
      var secret = speakeasy.generateSecret({length:20});
      const token = await generateOtp(secret.base32);

      if (req.body.userType && req.body.fullName && req.body.email && req.body.mobileNumber && req.body.password && req.body.companyName) {
        let vendorData = new VendorRegData({
          userType: req.body.userType,
          fullName: req.body.fullName,
          email: req.body.email,
          mobileNumber: req.body.mobileNumber,
          password: req.body.password,
          companyName: req.body.companyName,
          registrationNumber: req.body.userType + '_' + uniqid.process(),
          otpsecret: secret.base32
        });

        let saveVendor = await vendorData.save();

        const msg = {
          to: req.body.email,
          from: 'admin@ywc.in',
          subject: 'YWC OTP',
          text: 'Hello ' + req.body.fullName + ', Thank you for registering at YWC.in. Please enter the following OTP below to complete your activation: ' + token,
          html: 'Hello <strong>' + req.body.fullName + '</strong>, <br><br> Thank you for registering at YWC.in. Please enter the following OTP below to complete your activation:<strong> ' + token + ' </strong>',
        };

        sgMail.send(msg);

        res.json({ "success": true, "registrationNumber": vendorData.registrationNumber });
      } else {
        res.json({ success: false, message: 'Please fill the form properly' });
      }
    } catch (error) {
      if(error.code == 11000) {
        res.json({ success: false, message: 'That e-mail or mobile number is already taken'})
      } else {
        res.json({ success: false, message: error.message });
      }
    }
  });

  // Router to verify duplicate email
  router.post('/verifyEmail', async(req, res) => {
    try {
      const { email } = req.body;
      const emailRegex = RegExp(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

      if(!email){
        return res.status(422).json({ success: false, message: 'e-mail required!!' })
      }

      if(emailRegex.test(email)) {
        const vendorEmail = await VendorRegData.findOne({ "email": email.toLowerCase() });

        if(vendorEmail) {
          res.json({ success: false, message: 'The e-mail is already taken' });
        } else {
          res.json({ success: true, message: 'valid e-mail' });
        }
      } else {
        res.json({ success: false, message: 'invalid email address' });
      }
    } catch (error) {
      res.json({ success: false, message: error.message });
    }
  });

  // Router to verify duplicate phonenumber
  router.post('/verifyPhoneNumber', async(req, res) => {
    try {
      const { mobileNumber } = req.body;
      const mobileRegex = RegExp(/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/);

      if(!mobileNumber){
        res.status(422).json({ success: false, message: 'Mobile number required!!' });
      }

      if(mobileRegex.test(mobileNumber)) {
        const vendorPhonenumber = await VendorRegData.findOne({ "mobileNumber": mobileNumber });

        if(vendorPhonenumber) {
          res.json({ success: false, message: 'The Phone number is already taken' });
        } else {
          res.json({ success: true, message: 'valid Phone number' });
        }
      } else {
        res.json({ success: false, message: 'invalid phone number' });
      }
    } catch (error) {
      res.json({ success: false, message: error.message });
    }
  });

  // Router to verify OTP
  router.post('/verifyOTP', async(req, res) => {
    try {
      if(req.body.regNumber && req.body.token) {
        const Secret = await VendorRegData.findOne({ "registrationNumber" : req.body.regNumber});
        const token = req.body.token;
        let validOtp = await speakeasy.totp.verify({
          secret: Secret.otpsecret,
          encoding:'base32',
          token:token,
          digits:6,
          step: 60,
          window:2
        });

        if (validOtp) {
          res.json({ success: true, message: 'valid OTP'});
        } else {
          res.json({ success: false, message: 'Invalid OPT or OTP is expired' });
        }
      } else {
        res.json({ success: false, message: 'Something went wrong!!' });
      }
    } catch (error) {
      res.json({ success: false, message: error .message });
    }
  });

  // Router to re generate OPT
  router.post('/regenerateOTP', async(req, res) => {
    try {
      if(req.body.regNumber) {
        const user = await VendorRegData.findOne({ "registrationNumber" : req.body.regNumber});
        if(!user) {
          res.json({ success: false, message: 'No user found' });
        } else {
          const token = await generateOtp(user.otpsecret);

          const msg = {
            to: user.email,
            from: 'admin@ywc.in',
            subject: 'YWC regenerated OTP',
            text: 'Hello ' + user.fullName + ', Thank you for registering at YWC.in. Please enter the following OTP below to complete your activation: ' + token,
            html: 'Hello <strong>' + user.fullName + '</strong>, <br><br> Thank you for registering at YWC.in. Please enter the following OTP below to complete your activation:<strong> ' + token + ' </strong>',
          };

          sgMail.send(msg);
          res.json({ success: true, OTP: token, message: 'Regenerated OTP sent to your Registerd e-mail' });
        }
      } else {
        res.json({ success: false, message: 'Something went wrong'});
      }
    } catch (error) {
      res.json({ success: false, message: error.message });
    }
  });

  // Router for Company Details
  router.post('/companyDetails', async(req, res) => {
    try {
      const companyDetails = {
        MSME: req.body.msme,
        GST: req.body.gstNo,
        ROC: req.body.roc,
        SERVICE_TAX: req.body.serviceTaxNo,
        EXCISE_DUTY: req.body.exciseDuty,
        CST: req.body.cst,
        NSIC: req.body.nsic,
        DGS_D: req.body.dgsd,
        EEPC: req.body.eepc,
        IEC: req.body.iec,
        VAT_TIN: req.body.vatTin,
        PAN: req.body.panNumber
      }

      const registerdUser = await VendorRegData.findOneAndUpdate({ "registrationNumber": req.body.regNumber}, {$set: {"companyDetails": companyDetails }}, {new: true});
      if(registerdUser) {
        res.json({ success: true, userType: req.body.regNumber, user: registerdUser, message: 'successfull!!!' });
      } else {
        res.json({ success: false, message: 'No user found' });
      }
    } catch (error) {
      res.json({ success: false, message: error.message });
    }
  });

  // Router to verify GSTIN number
  router.post('/verifyGST', (req,res) => {
    const { gstNo, regNumber } = req.body;

    if (!gstNo) {
      return res.status(400).json({ success: false, message: 'GSTIN is required' });
    }

    axios.get('https://appyflow.in/api/verifyGST', {
      params: {
        gstNo: gstNo,
        key_secret: '7rCLlRjNWHNM9fheqnvjU5r6wVv1'
      }
    })
    .then(function (response) {
      if(response.data.error) {
        res.json({ success: false, message: response.data.message });
      } else {
        let vendor = new Vendordetails({
          gstDetails: response.data,
          registrationNumber: regNumber
        });
        vendor.save((err) => {
          if(err) {
            console.log(err);
          } else {
            res.json({ success: true, message: 'GSTIN is valid' });
          }
        });
      }
    })
    .catch(function (error) {
      res.json({ success: false, message: error });
    });
  });

  // Router for Bank account details
  router.post('/bankDetails', async(req, res) => {
    try {
      const { bankName, branchName, branchCode, ifsc, accountNumber, micrNo, regNumber } = req.body;

      if(!bankName || !branchName || !branchCode || !ifsc || !accountNumber || !micrNo){
        return res.status(400).json({ success: false, message: 'Please fill the form properly' });
      }

      const accInfo = {
        bank_name: bankName,
        branch_name: branchName,
        branch_code: branchCode,
        IFSC_code: ifsc,
        account_number: accountNumber,
        micr_No: micrNo
      }

      const accountHolder = await VendorRegData.findOneAndUpdate({ "registrationNumber": regNumber}, {$set: { "bankDetails": accInfo }}, { new: true});

      if(!accountHolder){
        return res.status(404).json({ success: false, message: 'No user found' });
      }

      res.json({ success: true, userType: regNumber, user: accountHolder, message: 'successfull' });
    } catch (error) {
      res.json({ success: false, message: error.message });
    }
  });

  // Router to upload user Documents while registration
  let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      const { regNumber } = req.body;
      const path = `/home/vn19689/upload/${Date.now()}`;

      fs.exists(path, exist => {
        if(!exist) {
          return fs.mkdir(path, error => cb(error, path))
        }
        return cb(null, path);
      })
    },

    filename: (req, file, cb) => {
      cb(null, file.originalname);
    }
  });

  let upload = multer({ storage: storage }).any();

  router.post('/uploadDocuments', (req, res) => {
    upload(req, res, async (err) => {
      if(err instanceof multer.MulterError){
        res.json({ success: false, response: err });
      } else if (err) {
        res.json({ success: false, response: err });
      }

      const documentPath = await VendorRegData.findOneAndUpdate({ "registrationNumber": req.body.regNumber}, {$set: { "uploadDocumentsPath": req.files[0].destination }}, { new: true});

      if(!documentPath){
        return res.status(404).json({ success: false, message: 'No user found' });
      }

      res.json({ success: true, user: documentPath, message: 'Files uploaded successfully' });
    });
  });

  // get All HSN codes
  router.get('/getAllHsn', (req, res) => {
    axios.get(`https://www.mastergst.com/gst-tools/commoncodes.json`)
    .then(response => {
      res.json({ message: "data from online json", data: response.data });
    })
    .catch(error => {
      const hsnAndGstListJson = './app/json/hsnwithgst.json';

      fs.readFile(hsnAndGstListJson, 'utf8', (err, data) => {
        if (err) { throw err; }
        let hsnJsonList = JSON.parse(data);
        res.json({ message: error.message, data: hsnJsonList });
      });
    })
  });

  // API to save HSN Codes
  router.post('/saveHsnCodes', async(req, res) => {
    try {
      const { hsncodes, regNumber } = req.body;

      if (!hsncodes && hsncodes.length <= 0) {
        return res.status(400).json({ success: false, message: 'please select HSN code' });
      }

      const hsnCodes = { hsnGst: hsncodes }
      const registerdUser = await VendorRegData.findOneAndUpdate({ "registrationNumber": regNumber}, {$set: {"hsnGst": hsnCodes }}, {new: true});

      if(!registerdUser) {
        return res.status(404).json({ success: false, message: 'No user found' });
      }

      res.json({ success: true, userType: regNumber, user: registerdUser, message: 'successfull' });
    } catch (error) {
      res.json({ success: false, message: error.message });
    }
  });

  // API to get GST rate by HSN codes
  router.post('/getGstByHsn', (req, res) => {
    const { hsnCodes } = req.body;

    if(hsnCodes instanceof Array && hsnCodes.length > 0) {
      axios.get(`https://www.mastergst.com/gst-tools/commoncodes.json`)
      .then(response => {
        const gstRates = response.data.filter(gst => {
          return hsnCodes.indexOf(gst.code) > -1
        })

        res.json({ success: true, data: gstRates });
      })
      .catch(error => {
        const hsnAndGstListJson = './app/json/hsnwithgst.json';

        fs.readFile(hsnAndGstListJson, 'utf8', (err, data) => {
          if(err) { throw err; }

          let jsonGstList = JSON.parse(data);

          const gstRates = jsonGstList.filter(gst => {
            return hsnCodes.indexOf(gst.code) > -1;
          });

          res.json({ success: true, data: gstRates });
        })
      });
    } else {
      res.json({ success: false, message: 'Valid HSN code required' });
    }
  });

  return router;
}
